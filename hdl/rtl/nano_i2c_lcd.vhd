library ieee;
use ieee.std_logic_1164.all;

entity nano_i2c_lcd is

  port (
    LED         : out   std_logic_vector(7 downto 0);
    SYS_CLK     : in    std_logic;      -- 50 MHz
    RST         : in    std_logic;
    START       : in    std_logic;
    SDA         : inout std_logic;
    SCL         : inout std_logic;
    SDRAM_ADDR  : out   std_logic_vector(12 downto 0);
    SDRAM_DQ    : inout std_logic_vector(15 downto 0);
    SDRAM_BA    : out   std_logic_vector(1 downto 0);
    SDRAM_DQM   : out   std_logic_vector(1 downto 0);
    SDRAM_RAS_N : out   std_logic;
    SDRAM_CAS_N : out   std_logic;
    SDRAM_CKE   : out   std_logic;
    SDRAM_CLK   : out   std_logic;
    SDRAM_WE_N  : out   std_logic;
    SDRAM_CS_N  : out   std_logic
    );

end entity nano_i2c_lcd;

architecture lcd_inst of nano_i2c_lcd is

  component nano_i2c_lcd_ss is
    port (
      clk_clk              : in    std_logic                     := 'X';
      i2cserialline_sda_in : in    std_logic                     := 'X';
      i2cserialline_scl_in : in    std_logic                     := 'X';
      i2cserialline_sda_oe : out   std_logic;
      i2cserialline_scl_oe : out   std_logic;
      ledcntrlreg_export   : out   std_logic_vector(7 downto 0);
      reset_reset_n        : in    std_logic                     := 'X';
      reset_async_export   : in    std_logic                     := 'X';
      sdramcntrlout_addr   : out   std_logic_vector(12 downto 0);
      sdramcntrlout_ba     : out   std_logic_vector(1 downto 0);
      sdramcntrlout_cas_n  : out   std_logic;
      sdramcntrlout_cke    : out   std_logic;
      sdramcntrlout_cs_n   : out   std_logic;
      sdramcntrlout_dq     : inout std_logic_vector(15 downto 0) := (others => 'X');
      sdramcntrlout_dqm    : out   std_logic_vector(1 downto 0);
      sdramcntrlout_ras_n  : out   std_logic;
      sdramcntrlout_we_n   : out   std_logic;
      statusreg_export     : out   std_logic_vector(7 downto 0);
      syspll_clk100_clk    : out   std_logic;
      syspll_locked_export : out   std_logic
      );
  end component nano_i2c_lcd_ss;
  
  signal rstFlop      : std_logic;
  signal statusRegVec : std_logic_vector(7 downto 0);
  signal clk100Mhz    : std_logic;
  signal sdaIn        : std_logic;
  signal sclIn        : std_logic;
  signal sdaOutEn     : std_logic;
  signal sclOutEn     : std_logic;
  signal pllLocked    : std_logic;

begin  -- architecture lcd_inst

  clk_process : process(SYS_CLK, RST)
  begin
    if(rising_edge(SYS_CLK)) then
      rstFlop <= RST;
    end if;
  end process clk_process;

  u0 : component nano_i2c_lcd_ss
    port map (
      clk_clk              => SYS_CLK,
      i2cserialline_sda_in => sdaIn,
      i2cserialline_scl_in => sclIn,
      i2cserialline_sda_oe => sdaOutEn,
      i2cserialline_scl_oe => sclOutEn,
      ledcntrlreg_export   => LED,
      reset_reset_n        => rstFlop,
      reset_async_export   => rstFlop,  --reset signal to pll
      sdramcntrlout_addr   => SDRAM_ADDR,
      sdramcntrlout_ba     => SDRAM_BA,
      sdramcntrlout_cas_n  => SDRAM_CAS_N,
      sdramcntrlout_cke    => SDRAM_CKE,
      sdramcntrlout_cs_n   => SDRAM_CS_N,
      sdramcntrlout_dq     => SDRAM_DQ,
      sdramcntrlout_dqm    => SDRAM_DQM,
      sdramcntrlout_ras_n  => SDRAM_RAS_N,
      sdramcntrlout_we_n   => SDRAM_WE_N,
      statusreg_export     => statusRegVec,
      syspll_clk100_clk    => clk100Mhz,
      syspll_locked_export => pllLocked
      );

  sdaIn <= SDA;
  SDA   <= '0' when sdaOutEn = '1' else 'Z';

  sclIn <= SCL;
  SCL   <= '0' when sclOutEn = '1' else 'Z';

end architecture lcd_inst;

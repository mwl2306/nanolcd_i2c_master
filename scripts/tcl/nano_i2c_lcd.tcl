# qsys scripting (.tcl) file for nano_i2c_lcd
package require -exact qsys 16.0

create_system {nano_i2c_lcd_ss}

set_project_property DEVICE_FAMILY {Cyclone IV E}
set_project_property DEVICE {EP4CE22F17C6}
set_project_property HIDE_FROM_IP_CATALOG {false}

# Instances and instance parameters
# (disabled instances are intentionally culled)
add_instance OCM altera_avalon_onchip_memory2 19.1
set_instance_parameter_value OCM {allowInSystemMemoryContentEditor} {0}
set_instance_parameter_value OCM {blockType} {AUTO}
set_instance_parameter_value OCM {copyInitFile} {0}
set_instance_parameter_value OCM {dataWidth} {32}
set_instance_parameter_value OCM {dataWidth2} {32}
set_instance_parameter_value OCM {dualPort} {0}
set_instance_parameter_value OCM {ecc_enabled} {0}
set_instance_parameter_value OCM {enPRInitMode} {0}
set_instance_parameter_value OCM {enableDiffWidth} {0}
set_instance_parameter_value OCM {initMemContent} {1}
set_instance_parameter_value OCM {initializationFileName} {onchip_mem.hex}
set_instance_parameter_value OCM {instanceID} {NONE}
set_instance_parameter_value OCM {memorySize} {16384.0}
set_instance_parameter_value OCM {readDuringWriteMode} {DONT_CARE}
set_instance_parameter_value OCM {resetrequest_enabled} {1}
set_instance_parameter_value OCM {simAllowMRAMContentsFile} {0}
set_instance_parameter_value OCM {simMemInitOnlyFilename} {0}
set_instance_parameter_value OCM {singleClockOperation} {0}
set_instance_parameter_value OCM {slave1Latency} {1}
set_instance_parameter_value OCM {slave2Latency} {1}
set_instance_parameter_value OCM {useNonDefaultInitFile} {0}
set_instance_parameter_value OCM {useShallowMemBlocks} {0}
set_instance_parameter_value OCM {writable} {1}

add_instance cpuN2 altera_nios2_gen2 19.1
set_instance_parameter_value cpuN2 {bht_ramBlockType} {Automatic}
set_instance_parameter_value cpuN2 {breakOffset} {32}
set_instance_parameter_value cpuN2 {breakSlave} {None}
set_instance_parameter_value cpuN2 {cdx_enabled} {0}
set_instance_parameter_value cpuN2 {cpuArchRev} {1}
set_instance_parameter_value cpuN2 {cpuID} {0}
set_instance_parameter_value cpuN2 {cpuReset} {0}
set_instance_parameter_value cpuN2 {data_master_high_performance_paddr_base} {0}
set_instance_parameter_value cpuN2 {data_master_high_performance_paddr_size} {0.0}
set_instance_parameter_value cpuN2 {data_master_paddr_base} {0}
set_instance_parameter_value cpuN2 {data_master_paddr_size} {0.0}
set_instance_parameter_value cpuN2 {dcache_bursts} {false}
set_instance_parameter_value cpuN2 {dcache_numTCDM} {0}
set_instance_parameter_value cpuN2 {dcache_ramBlockType} {Automatic}
set_instance_parameter_value cpuN2 {dcache_size} {2048}
set_instance_parameter_value cpuN2 {dcache_tagramBlockType} {Automatic}
set_instance_parameter_value cpuN2 {dcache_victim_buf_impl} {ram}
set_instance_parameter_value cpuN2 {debug_OCIOnchipTrace} {_128}
set_instance_parameter_value cpuN2 {debug_assignJtagInstanceID} {0}
set_instance_parameter_value cpuN2 {debug_datatrigger} {0}
set_instance_parameter_value cpuN2 {debug_debugReqSignals} {0}
set_instance_parameter_value cpuN2 {debug_enabled} {1}
set_instance_parameter_value cpuN2 {debug_hwbreakpoint} {0}
set_instance_parameter_value cpuN2 {debug_jtagInstanceID} {0}
set_instance_parameter_value cpuN2 {debug_traceStorage} {onchip_trace}
set_instance_parameter_value cpuN2 {debug_traceType} {none}
set_instance_parameter_value cpuN2 {debug_triggerArming} {1}
set_instance_parameter_value cpuN2 {dividerType} {no_div}
set_instance_parameter_value cpuN2 {exceptionOffset} {32}
set_instance_parameter_value cpuN2 {exceptionSlave} {sdramCntrl.s1}
set_instance_parameter_value cpuN2 {fa_cache_line} {2}
set_instance_parameter_value cpuN2 {fa_cache_linesize} {0}
set_instance_parameter_value cpuN2 {flash_instruction_master_paddr_base} {0}
set_instance_parameter_value cpuN2 {flash_instruction_master_paddr_size} {0.0}
set_instance_parameter_value cpuN2 {icache_burstType} {None}
set_instance_parameter_value cpuN2 {icache_numTCIM} {0}
set_instance_parameter_value cpuN2 {icache_ramBlockType} {Automatic}
set_instance_parameter_value cpuN2 {icache_size} {4096}
set_instance_parameter_value cpuN2 {icache_tagramBlockType} {Automatic}
set_instance_parameter_value cpuN2 {impl} {Tiny}
set_instance_parameter_value cpuN2 {instruction_master_high_performance_paddr_base} {0}
set_instance_parameter_value cpuN2 {instruction_master_high_performance_paddr_size} {0.0}
set_instance_parameter_value cpuN2 {instruction_master_paddr_base} {0}
set_instance_parameter_value cpuN2 {instruction_master_paddr_size} {0.0}
set_instance_parameter_value cpuN2 {io_regionbase} {0}
set_instance_parameter_value cpuN2 {io_regionsize} {0}
set_instance_parameter_value cpuN2 {master_addr_map} {0}
set_instance_parameter_value cpuN2 {mmu_TLBMissExcOffset} {0}
set_instance_parameter_value cpuN2 {mmu_TLBMissExcSlave} {None}
set_instance_parameter_value cpuN2 {mmu_autoAssignTlbPtrSz} {1}
set_instance_parameter_value cpuN2 {mmu_enabled} {0}
set_instance_parameter_value cpuN2 {mmu_processIDNumBits} {8}
set_instance_parameter_value cpuN2 {mmu_ramBlockType} {Automatic}
set_instance_parameter_value cpuN2 {mmu_tlbNumWays} {16}
set_instance_parameter_value cpuN2 {mmu_tlbPtrSz} {7}
set_instance_parameter_value cpuN2 {mmu_udtlbNumEntries} {6}
set_instance_parameter_value cpuN2 {mmu_uitlbNumEntries} {4}
set_instance_parameter_value cpuN2 {mpu_enabled} {0}
set_instance_parameter_value cpuN2 {mpu_minDataRegionSize} {12}
set_instance_parameter_value cpuN2 {mpu_minInstRegionSize} {12}
set_instance_parameter_value cpuN2 {mpu_numOfDataRegion} {8}
set_instance_parameter_value cpuN2 {mpu_numOfInstRegion} {8}
set_instance_parameter_value cpuN2 {mpu_useLimit} {0}
set_instance_parameter_value cpuN2 {mpx_enabled} {0}
set_instance_parameter_value cpuN2 {mul_32_impl} {2}
set_instance_parameter_value cpuN2 {mul_64_impl} {0}
set_instance_parameter_value cpuN2 {mul_shift_choice} {0}
set_instance_parameter_value cpuN2 {ocimem_ramBlockType} {Automatic}
set_instance_parameter_value cpuN2 {ocimem_ramInit} {0}
set_instance_parameter_value cpuN2 {regfile_ramBlockType} {Automatic}
set_instance_parameter_value cpuN2 {register_file_por} {0}
set_instance_parameter_value cpuN2 {resetOffset} {0}
set_instance_parameter_value cpuN2 {resetSlave} {sdramCntrl.s1}
set_instance_parameter_value cpuN2 {resetrequest_enabled} {1}
set_instance_parameter_value cpuN2 {setting_HBreakTest} {0}
set_instance_parameter_value cpuN2 {setting_HDLSimCachesCleared} {1}
set_instance_parameter_value cpuN2 {setting_activateMonitors} {1}
set_instance_parameter_value cpuN2 {setting_activateTestEndChecker} {0}
set_instance_parameter_value cpuN2 {setting_activateTrace} {0}
set_instance_parameter_value cpuN2 {setting_allow_break_inst} {0}
set_instance_parameter_value cpuN2 {setting_alwaysEncrypt} {1}
set_instance_parameter_value cpuN2 {setting_asic_add_scan_mode_input} {0}
set_instance_parameter_value cpuN2 {setting_asic_enabled} {0}
set_instance_parameter_value cpuN2 {setting_asic_synopsys_translate_on_off} {0}
set_instance_parameter_value cpuN2 {setting_asic_third_party_synthesis} {0}
set_instance_parameter_value cpuN2 {setting_avalonDebugPortPresent} {0}
set_instance_parameter_value cpuN2 {setting_bhtPtrSz} {8}
set_instance_parameter_value cpuN2 {setting_bigEndian} {0}
set_instance_parameter_value cpuN2 {setting_branchpredictiontype} {Dynamic}
set_instance_parameter_value cpuN2 {setting_breakslaveoveride} {0}
set_instance_parameter_value cpuN2 {setting_clearXBitsLDNonBypass} {1}
set_instance_parameter_value cpuN2 {setting_dc_ecc_present} {1}
set_instance_parameter_value cpuN2 {setting_disable_tmr_inj} {0}
set_instance_parameter_value cpuN2 {setting_disableocitrace} {0}
set_instance_parameter_value cpuN2 {setting_dtcm_ecc_present} {1}
set_instance_parameter_value cpuN2 {setting_ecc_present} {0}
set_instance_parameter_value cpuN2 {setting_ecc_sim_test_ports} {0}
set_instance_parameter_value cpuN2 {setting_exportHostDebugPort} {0}
set_instance_parameter_value cpuN2 {setting_exportPCB} {0}
set_instance_parameter_value cpuN2 {setting_export_large_RAMs} {0}
set_instance_parameter_value cpuN2 {setting_exportdebuginfo} {0}
set_instance_parameter_value cpuN2 {setting_exportvectors} {0}
set_instance_parameter_value cpuN2 {setting_fast_register_read} {0}
set_instance_parameter_value cpuN2 {setting_ic_ecc_present} {1}
set_instance_parameter_value cpuN2 {setting_interruptControllerType} {Internal}
set_instance_parameter_value cpuN2 {setting_itcm_ecc_present} {1}
set_instance_parameter_value cpuN2 {setting_mmu_ecc_present} {1}
set_instance_parameter_value cpuN2 {setting_oci_export_jtag_signals} {0}
set_instance_parameter_value cpuN2 {setting_oci_version} {1}
set_instance_parameter_value cpuN2 {setting_preciseIllegalMemAccessException} {0}
set_instance_parameter_value cpuN2 {setting_removeRAMinit} {0}
set_instance_parameter_value cpuN2 {setting_rf_ecc_present} {1}
set_instance_parameter_value cpuN2 {setting_shadowRegisterSets} {0}
set_instance_parameter_value cpuN2 {setting_showInternalSettings} {0}
set_instance_parameter_value cpuN2 {setting_showUnpublishedSettings} {0}
set_instance_parameter_value cpuN2 {setting_support31bitdcachebypass} {1}
set_instance_parameter_value cpuN2 {setting_tmr_output_disable} {0}
set_instance_parameter_value cpuN2 {setting_usedesignware} {0}
set_instance_parameter_value cpuN2 {shift_rot_impl} {1}
set_instance_parameter_value cpuN2 {tightly_coupled_data_master_0_paddr_base} {0}
set_instance_parameter_value cpuN2 {tightly_coupled_data_master_0_paddr_size} {0.0}
set_instance_parameter_value cpuN2 {tightly_coupled_data_master_1_paddr_base} {0}
set_instance_parameter_value cpuN2 {tightly_coupled_data_master_1_paddr_size} {0.0}
set_instance_parameter_value cpuN2 {tightly_coupled_data_master_2_paddr_base} {0}
set_instance_parameter_value cpuN2 {tightly_coupled_data_master_2_paddr_size} {0.0}
set_instance_parameter_value cpuN2 {tightly_coupled_data_master_3_paddr_base} {0}
set_instance_parameter_value cpuN2 {tightly_coupled_data_master_3_paddr_size} {0.0}
set_instance_parameter_value cpuN2 {tightly_coupled_instruction_master_0_paddr_base} {0}
set_instance_parameter_value cpuN2 {tightly_coupled_instruction_master_0_paddr_size} {0.0}
set_instance_parameter_value cpuN2 {tightly_coupled_instruction_master_1_paddr_base} {0}
set_instance_parameter_value cpuN2 {tightly_coupled_instruction_master_1_paddr_size} {0.0}
set_instance_parameter_value cpuN2 {tightly_coupled_instruction_master_2_paddr_base} {0}
set_instance_parameter_value cpuN2 {tightly_coupled_instruction_master_2_paddr_size} {0.0}
set_instance_parameter_value cpuN2 {tightly_coupled_instruction_master_3_paddr_base} {0}
set_instance_parameter_value cpuN2 {tightly_coupled_instruction_master_3_paddr_size} {0.0}
set_instance_parameter_value cpuN2 {tmr_enabled} {0}
set_instance_parameter_value cpuN2 {tracefilename} {}
set_instance_parameter_value cpuN2 {userDefinedSettings} {}

add_instance i2cMaster altera_avalon_i2c 19.1
set_instance_parameter_value i2cMaster {FIFO_DEPTH} {128}
set_instance_parameter_value i2cMaster {USE_AV_ST} {0}

add_instance jtagUART altera_avalon_jtag_uart 19.1
set_instance_parameter_value jtagUART {allowMultipleConnections} {0}
set_instance_parameter_value jtagUART {hubInstanceID} {0}
set_instance_parameter_value jtagUART {readBufferDepth} {64}
set_instance_parameter_value jtagUART {readIRQThreshold} {8}
set_instance_parameter_value jtagUART {simInputCharacterStream} {}
set_instance_parameter_value jtagUART {simInteractiveOptions} {NO_INTERACTIVE_WINDOWS}
set_instance_parameter_value jtagUART {useRegistersForReadBuffer} {0}
set_instance_parameter_value jtagUART {useRegistersForWriteBuffer} {0}
set_instance_parameter_value jtagUART {useRelativePathForSimFile} {0}
set_instance_parameter_value jtagUART {writeBufferDepth} {64}
set_instance_parameter_value jtagUART {writeIRQThreshold} {8}

add_instance ledCntrlReg altera_avalon_pio 19.1
set_instance_parameter_value ledCntrlReg {bitClearingEdgeCapReg} {0}
set_instance_parameter_value ledCntrlReg {bitModifyingOutReg} {0}
set_instance_parameter_value ledCntrlReg {captureEdge} {0}
set_instance_parameter_value ledCntrlReg {direction} {Output}
set_instance_parameter_value ledCntrlReg {edgeType} {RISING}
set_instance_parameter_value ledCntrlReg {generateIRQ} {0}
set_instance_parameter_value ledCntrlReg {irqType} {LEVEL}
set_instance_parameter_value ledCntrlReg {resetValue} {0.0}
set_instance_parameter_value ledCntrlReg {simDoTestBenchWiring} {0}
set_instance_parameter_value ledCntrlReg {simDrivenValue} {0.0}
set_instance_parameter_value ledCntrlReg {width} {8}

add_instance sdramClk altera_up_avalon_sys_sdram_pll 18.0
set_instance_parameter_value sdramClk {CIII_boards} {DE0}
set_instance_parameter_value sdramClk {CIV_boards} {DE0-Nano}
set_instance_parameter_value sdramClk {CV_boards} {DE10-Standard}
set_instance_parameter_value sdramClk {MAX10_boards} {DE10-Lite}
set_instance_parameter_value sdramClk {gui_outclk} {50.0}
set_instance_parameter_value sdramClk {gui_refclk} {50.0}
set_instance_parameter_value sdramClk {other_boards} {None}

add_instance sdramCntrl altera_avalon_new_sdram_controller 19.1
set_instance_parameter_value sdramCntrl {TAC} {5.5}
set_instance_parameter_value sdramCntrl {TMRD} {3.0}
set_instance_parameter_value sdramCntrl {TRCD} {20.0}
set_instance_parameter_value sdramCntrl {TRFC} {70.0}
set_instance_parameter_value sdramCntrl {TRP} {20.0}
set_instance_parameter_value sdramCntrl {TWR} {14.0}
set_instance_parameter_value sdramCntrl {casLatency} {3}
set_instance_parameter_value sdramCntrl {columnWidth} {9}
set_instance_parameter_value sdramCntrl {dataWidth} {16}
set_instance_parameter_value sdramCntrl {generateSimulationModel} {0}
set_instance_parameter_value sdramCntrl {initNOPDelay} {0.0}
set_instance_parameter_value sdramCntrl {initRefreshCommands} {8}
set_instance_parameter_value sdramCntrl {masteredTristateBridgeSlave} {0}
set_instance_parameter_value sdramCntrl {model} {single_Micron_MT48LC4M32B2_7_chip}
set_instance_parameter_value sdramCntrl {numberOfBanks} {4}
set_instance_parameter_value sdramCntrl {numberOfChipSelects} {1}
set_instance_parameter_value sdramCntrl {pinsSharedViaTriState} {0}
set_instance_parameter_value sdramCntrl {powerUpDelay} {200.0}
set_instance_parameter_value sdramCntrl {refreshPeriod} {7.813}
set_instance_parameter_value sdramCntrl {registerDataIn} {1}
set_instance_parameter_value sdramCntrl {rowWidth} {13}

add_instance statusReg altera_avalon_pio 19.1
set_instance_parameter_value statusReg {bitClearingEdgeCapReg} {0}
set_instance_parameter_value statusReg {bitModifyingOutReg} {0}
set_instance_parameter_value statusReg {captureEdge} {0}
set_instance_parameter_value statusReg {direction} {Output}
set_instance_parameter_value statusReg {edgeType} {RISING}
set_instance_parameter_value statusReg {generateIRQ} {0}
set_instance_parameter_value statusReg {irqType} {LEVEL}
set_instance_parameter_value statusReg {resetValue} {0.0}
set_instance_parameter_value statusReg {simDoTestBenchWiring} {0}
set_instance_parameter_value statusReg {simDrivenValue} {0.0}
set_instance_parameter_value statusReg {width} {8}

add_instance sysClkIn clock_source 19.1
set_instance_parameter_value sysClkIn {clockFrequency} {50000000.0}
set_instance_parameter_value sysClkIn {clockFrequencyKnown} {1}
set_instance_parameter_value sysClkIn {resetSynchronousEdges} {NONE}

add_instance sysPLL altpll 19.1
set_instance_parameter_value sysPLL {AVALON_USE_SEPARATE_SYSCLK} {NO}
set_instance_parameter_value sysPLL {BANDWIDTH} {}
set_instance_parameter_value sysPLL {BANDWIDTH_TYPE} {AUTO}
set_instance_parameter_value sysPLL {CLK0_DIVIDE_BY} {1}
set_instance_parameter_value sysPLL {CLK0_DUTY_CYCLE} {50}
set_instance_parameter_value sysPLL {CLK0_MULTIPLY_BY} {2}
set_instance_parameter_value sysPLL {CLK0_PHASE_SHIFT} {0}
set_instance_parameter_value sysPLL {CLK1_DIVIDE_BY} {1}
set_instance_parameter_value sysPLL {CLK1_DUTY_CYCLE} {50}
set_instance_parameter_value sysPLL {CLK1_MULTIPLY_BY} {2}
set_instance_parameter_value sysPLL {CLK1_PHASE_SHIFT} {0}
set_instance_parameter_value sysPLL {CLK2_DIVIDE_BY} {}
set_instance_parameter_value sysPLL {CLK2_DUTY_CYCLE} {}
set_instance_parameter_value sysPLL {CLK2_MULTIPLY_BY} {}
set_instance_parameter_value sysPLL {CLK2_PHASE_SHIFT} {}
set_instance_parameter_value sysPLL {CLK3_DIVIDE_BY} {}
set_instance_parameter_value sysPLL {CLK3_DUTY_CYCLE} {}
set_instance_parameter_value sysPLL {CLK3_MULTIPLY_BY} {}
set_instance_parameter_value sysPLL {CLK3_PHASE_SHIFT} {}
set_instance_parameter_value sysPLL {CLK4_DIVIDE_BY} {}
set_instance_parameter_value sysPLL {CLK4_DUTY_CYCLE} {}
set_instance_parameter_value sysPLL {CLK4_MULTIPLY_BY} {}
set_instance_parameter_value sysPLL {CLK4_PHASE_SHIFT} {}
set_instance_parameter_value sysPLL {CLK5_DIVIDE_BY} {}
set_instance_parameter_value sysPLL {CLK5_DUTY_CYCLE} {}
set_instance_parameter_value sysPLL {CLK5_MULTIPLY_BY} {}
set_instance_parameter_value sysPLL {CLK5_PHASE_SHIFT} {}
set_instance_parameter_value sysPLL {CLK6_DIVIDE_BY} {}
set_instance_parameter_value sysPLL {CLK6_DUTY_CYCLE} {}
set_instance_parameter_value sysPLL {CLK6_MULTIPLY_BY} {}
set_instance_parameter_value sysPLL {CLK6_PHASE_SHIFT} {}
set_instance_parameter_value sysPLL {CLK7_DIVIDE_BY} {}
set_instance_parameter_value sysPLL {CLK7_DUTY_CYCLE} {}
set_instance_parameter_value sysPLL {CLK7_MULTIPLY_BY} {}
set_instance_parameter_value sysPLL {CLK7_PHASE_SHIFT} {}
set_instance_parameter_value sysPLL {CLK8_DIVIDE_BY} {}
set_instance_parameter_value sysPLL {CLK8_DUTY_CYCLE} {}
set_instance_parameter_value sysPLL {CLK8_MULTIPLY_BY} {}
set_instance_parameter_value sysPLL {CLK8_PHASE_SHIFT} {}
set_instance_parameter_value sysPLL {CLK9_DIVIDE_BY} {}
set_instance_parameter_value sysPLL {CLK9_DUTY_CYCLE} {}
set_instance_parameter_value sysPLL {CLK9_MULTIPLY_BY} {}
set_instance_parameter_value sysPLL {CLK9_PHASE_SHIFT} {}
set_instance_parameter_value sysPLL {COMPENSATE_CLOCK} {CLK0}
set_instance_parameter_value sysPLL {DOWN_SPREAD} {}
set_instance_parameter_value sysPLL {DPA_DIVIDER} {}
set_instance_parameter_value sysPLL {DPA_DIVIDE_BY} {}
set_instance_parameter_value sysPLL {DPA_MULTIPLY_BY} {}
set_instance_parameter_value sysPLL {ENABLE_SWITCH_OVER_COUNTER} {}
set_instance_parameter_value sysPLL {EXTCLK0_DIVIDE_BY} {}
set_instance_parameter_value sysPLL {EXTCLK0_DUTY_CYCLE} {}
set_instance_parameter_value sysPLL {EXTCLK0_MULTIPLY_BY} {}
set_instance_parameter_value sysPLL {EXTCLK0_PHASE_SHIFT} {}
set_instance_parameter_value sysPLL {EXTCLK1_DIVIDE_BY} {}
set_instance_parameter_value sysPLL {EXTCLK1_DUTY_CYCLE} {}
set_instance_parameter_value sysPLL {EXTCLK1_MULTIPLY_BY} {}
set_instance_parameter_value sysPLL {EXTCLK1_PHASE_SHIFT} {}
set_instance_parameter_value sysPLL {EXTCLK2_DIVIDE_BY} {}
set_instance_parameter_value sysPLL {EXTCLK2_DUTY_CYCLE} {}
set_instance_parameter_value sysPLL {EXTCLK2_MULTIPLY_BY} {}
set_instance_parameter_value sysPLL {EXTCLK2_PHASE_SHIFT} {}
set_instance_parameter_value sysPLL {EXTCLK3_DIVIDE_BY} {}
set_instance_parameter_value sysPLL {EXTCLK3_DUTY_CYCLE} {}
set_instance_parameter_value sysPLL {EXTCLK3_MULTIPLY_BY} {}
set_instance_parameter_value sysPLL {EXTCLK3_PHASE_SHIFT} {}
set_instance_parameter_value sysPLL {FEEDBACK_SOURCE} {}
set_instance_parameter_value sysPLL {GATE_LOCK_COUNTER} {}
set_instance_parameter_value sysPLL {GATE_LOCK_SIGNAL} {}
set_instance_parameter_value sysPLL {HIDDEN_CONSTANTS} {CT#PORT_clk5 PORT_UNUSED CT#PORT_clk4 PORT_UNUSED CT#PORT_clk3 PORT_UNUSED CT#PORT_clk2 PORT_UNUSED CT#PORT_clk1 PORT_USED CT#PORT_clk0 PORT_USED CT#CLK0_MULTIPLY_BY 2 CT#PORT_SCANWRITE PORT_UNUSED CT#PORT_SCANACLR PORT_UNUSED CT#PORT_PFDENA PORT_UNUSED CT#PORT_PLLENA PORT_UNUSED CT#PORT_SCANDATA PORT_UNUSED CT#PORT_SCANCLKENA PORT_UNUSED CT#WIDTH_CLOCK 5 CT#PORT_SCANDATAOUT PORT_UNUSED CT#LPM_TYPE altpll CT#PLL_TYPE AUTO CT#CLK0_PHASE_SHIFT 0 CT#CLK1_DUTY_CYCLE 50 CT#PORT_PHASEDONE PORT_UNUSED CT#OPERATION_MODE NORMAL CT#PORT_CONFIGUPDATE PORT_UNUSED CT#CLK1_MULTIPLY_BY 2 CT#COMPENSATE_CLOCK CLK0 CT#PORT_CLKSWITCH PORT_UNUSED CT#INCLK0_INPUT_FREQUENCY 20000 CT#PORT_SCANDONE PORT_UNUSED CT#PORT_CLKLOSS PORT_UNUSED CT#PORT_INCLK1 PORT_UNUSED CT#AVALON_USE_SEPARATE_SYSCLK NO CT#PORT_INCLK0 PORT_USED CT#PORT_clkena5 PORT_UNUSED CT#PORT_clkena4 PORT_UNUSED CT#PORT_clkena3 PORT_UNUSED CT#PORT_clkena2 PORT_UNUSED CT#PORT_clkena1 PORT_UNUSED CT#PORT_clkena0 PORT_UNUSED CT#CLK1_PHASE_SHIFT 0 CT#PORT_ARESET PORT_USED CT#BANDWIDTH_TYPE AUTO CT#INTENDED_DEVICE_FAMILY {Cyclone IV E} CT#PORT_SCANREAD PORT_UNUSED CT#PORT_PHASESTEP PORT_UNUSED CT#PORT_SCANCLK PORT_UNUSED CT#PORT_CLKBAD1 PORT_UNUSED CT#PORT_CLKBAD0 PORT_UNUSED CT#PORT_FBIN PORT_UNUSED CT#PORT_PHASEUPDOWN PORT_UNUSED CT#PORT_extclk3 PORT_UNUSED CT#PORT_extclk2 PORT_UNUSED CT#PORT_extclk1 PORT_UNUSED CT#PORT_PHASECOUNTERSELECT PORT_UNUSED CT#PORT_extclk0 PORT_UNUSED CT#PORT_ACTIVECLOCK PORT_UNUSED CT#CLK0_DUTY_CYCLE 50 CT#CLK0_DIVIDE_BY 1 CT#CLK1_DIVIDE_BY 1 CT#PORT_LOCKED PORT_USED}
set_instance_parameter_value sysPLL {HIDDEN_CUSTOM_ELABORATION} {altpll_avalon_elaboration}
set_instance_parameter_value sysPLL {HIDDEN_CUSTOM_POST_EDIT} {altpll_avalon_post_edit}
set_instance_parameter_value sysPLL {HIDDEN_IF_PORTS} {IF#phasecounterselect {input 4} IF#locked {output 0} IF#reset {input 0} IF#clk {input 0} IF#phaseupdown {input 0} IF#scandone {output 0} IF#readdata {output 32} IF#write {input 0} IF#scanclk {input 0} IF#phasedone {output 0} IF#address {input 2} IF#c1 {output 0} IF#c0 {output 0} IF#writedata {input 32} IF#read {input 0} IF#areset {input 0} IF#scanclkena {input 0} IF#scandataout {output 0} IF#configupdate {input 0} IF#phasestep {input 0} IF#scandata {input 0}}
set_instance_parameter_value sysPLL {HIDDEN_IS_FIRST_EDIT} {0}
set_instance_parameter_value sysPLL {HIDDEN_IS_NUMERIC} {IN#WIDTH_CLOCK 1 IN#CLK0_DUTY_CYCLE 1 IN#PLL_TARGET_HARCOPY_CHECK 1 IN#CLK1_MULTIPLY_BY 1 IN#SWITCHOVER_COUNT_EDIT 1 IN#INCLK0_INPUT_FREQUENCY 1 IN#PLL_LVDS_PLL_CHECK 1 IN#PLL_AUTOPLL_CHECK 1 IN#PLL_FASTPLL_CHECK 1 IN#CLK1_DUTY_CYCLE 1 IN#PLL_ENHPLL_CHECK 1 IN#DIV_FACTOR1 1 IN#DIV_FACTOR0 1 IN#LVDS_MODE_DATA_RATE_DIRTY 1 IN#GLOCK_COUNTER_EDIT 1 IN#CLK0_DIVIDE_BY 1 IN#MULT_FACTOR1 1 IN#MULT_FACTOR0 1 IN#CLK0_MULTIPLY_BY 1 IN#USE_MIL_SPEED_GRADE 1 IN#CLK1_DIVIDE_BY 1}
set_instance_parameter_value sysPLL {HIDDEN_MF_PORTS} {MF#areset 1 MF#clk 1 MF#locked 1 MF#inclk 1}
set_instance_parameter_value sysPLL {HIDDEN_PRIVATES} {PT#GLOCKED_FEATURE_ENABLED 0 PT#SPREAD_FEATURE_ENABLED 0 PT#BANDWIDTH_FREQ_UNIT MHz PT#CUR_DEDICATED_CLK c0 PT#INCLK0_FREQ_EDIT 50.000 PT#BANDWIDTH_PRESET Low PT#PLL_LVDS_PLL_CHECK 0 PT#BANDWIDTH_USE_PRESET 0 PT#AVALON_USE_SEPARATE_SYSCLK NO PT#PLL_ENHPLL_CHECK 0 PT#OUTPUT_FREQ_UNIT1 MHz PT#OUTPUT_FREQ_UNIT0 MHz PT#PHASE_RECONFIG_FEATURE_ENABLED 1 PT#CREATE_CLKBAD_CHECK 0 PT#CLKSWITCH_CHECK 0 PT#INCLK1_FREQ_EDIT 100.000 PT#NORMAL_MODE_RADIO 1 PT#SRC_SYNCH_COMP_RADIO 0 PT#PLL_ARESET_CHECK 1 PT#LONG_SCAN_RADIO 1 PT#SCAN_FEATURE_ENABLED 1 PT#PHASE_RECONFIG_INPUTS_CHECK 0 PT#USE_CLK1 1 PT#USE_CLK0 1 PT#PRIMARY_CLK_COMBO inclk0 PT#BANDWIDTH 1.000 PT#GLOCKED_COUNTER_EDIT_CHANGED 1 PT#PLL_FASTPLL_CHECK 0 PT#SPREAD_FREQ_UNIT KHz PT#PLL_AUTOPLL_CHECK 1 PT#LVDS_PHASE_SHIFT_UNIT1 deg PT#LVDS_PHASE_SHIFT_UNIT0 deg PT#OUTPUT_FREQ_MODE1 1 PT#SWITCHOVER_FEATURE_ENABLED 0 PT#MIG_DEVICE_SPEED_GRADE Any PT#OUTPUT_FREQ_MODE0 1 PT#BANDWIDTH_FEATURE_ENABLED 1 PT#INCLK0_FREQ_UNIT_COMBO MHz PT#ZERO_DELAY_RADIO 0 PT#OUTPUT_FREQ1 100.00000000 PT#OUTPUT_FREQ0 100.00000000 PT#SHORT_SCAN_RADIO 0 PT#LVDS_MODE_DATA_RATE_DIRTY 0 PT#CUR_FBIN_CLK c0 PT#PLL_ADVANCED_PARAM_CHECK 0 PT#CLKBAD_SWITCHOVER_CHECK 0 PT#PHASE_SHIFT_STEP_ENABLED_CHECK 0 PT#DEVICE_SPEED_GRADE 6 PT#PLL_FBMIMIC_CHECK 0 PT#LVDS_MODE_DATA_RATE {Not Available} PT#LOCKED_OUTPUT_CHECK 1 PT#SPREAD_PERCENT 0.500 PT#PHASE_SHIFT1 0.00000000 PT#PHASE_SHIFT0 0.00000000 PT#DIV_FACTOR1 1 PT#DIV_FACTOR0 1 PT#CNX_NO_COMPENSATE_RADIO 0 PT#USE_CLKENA1 0 PT#USE_CLKENA0 0 PT#CREATE_INCLK1_CHECK 0 PT#GLOCK_COUNTER_EDIT 1048575 PT#INCLK1_FREQ_UNIT_COMBO MHz PT#EFF_OUTPUT_FREQ_VALUE1 100.000000 PT#EFF_OUTPUT_FREQ_VALUE0 100.000000 PT#SPREAD_FREQ 50.000 PT#USE_MIL_SPEED_GRADE 0 PT#EXPLICIT_SWITCHOVER_COUNTER 0 PT#STICKY_CLK1 1 PT#STICKY_CLK0 1 PT#EXT_FEEDBACK_RADIO 0 PT#MIRROR_CLK1 0 PT#MIRROR_CLK0 0 PT#SWITCHOVER_COUNT_EDIT 1 PT#SELF_RESET_LOCK_LOSS 0 PT#PLL_PFDENA_CHECK 0 PT#INT_FEEDBACK__MODE_RADIO 1 PT#INCLK1_FREQ_EDIT_CHANGED 1 PT#CLKLOSS_CHECK 0 PT#SYNTH_WRAPPER_GEN_POSTFIX 0 PT#PHASE_SHIFT_UNIT1 deg PT#PHASE_SHIFT_UNIT0 deg PT#BANDWIDTH_USE_AUTO 1 PT#HAS_MANUAL_SWITCHOVER 1 PT#MULT_FACTOR1 1 PT#MULT_FACTOR0 1 PT#SPREAD_USE 0 PT#GLOCKED_MODE_CHECK 0 PT#SACN_INPUTS_CHECK 0 PT#DUTY_CYCLE1 50.00000000 PT#INTENDED_DEVICE_FAMILY {Cyclone IV E} PT#DUTY_CYCLE0 50.00000000 PT#PLL_TARGET_HARCOPY_CHECK 0 PT#INCLK1_FREQ_UNIT_CHANGED 1 PT#RECONFIG_FILE ALTPLL1587184824892753.mif PT#ACTIVECLK_CHECK 0}
set_instance_parameter_value sysPLL {HIDDEN_USED_PORTS} {UP#locked used UP#c1 used UP#c0 used UP#areset used UP#inclk0 used}
set_instance_parameter_value sysPLL {INCLK0_INPUT_FREQUENCY} {20000}
set_instance_parameter_value sysPLL {INCLK1_INPUT_FREQUENCY} {}
set_instance_parameter_value sysPLL {INTENDED_DEVICE_FAMILY} {Cyclone IV E}
set_instance_parameter_value sysPLL {INVALID_LOCK_MULTIPLIER} {}
set_instance_parameter_value sysPLL {LOCK_HIGH} {}
set_instance_parameter_value sysPLL {LOCK_LOW} {}
set_instance_parameter_value sysPLL {OPERATION_MODE} {NORMAL}
set_instance_parameter_value sysPLL {PLL_TYPE} {AUTO}
set_instance_parameter_value sysPLL {PORT_ACTIVECLOCK} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_ARESET} {PORT_USED}
set_instance_parameter_value sysPLL {PORT_CLKBAD0} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_CLKBAD1} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_CLKLOSS} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_CLKSWITCH} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_CONFIGUPDATE} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_ENABLE0} {}
set_instance_parameter_value sysPLL {PORT_ENABLE1} {}
set_instance_parameter_value sysPLL {PORT_FBIN} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_FBOUT} {}
set_instance_parameter_value sysPLL {PORT_INCLK0} {PORT_USED}
set_instance_parameter_value sysPLL {PORT_INCLK1} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_LOCKED} {PORT_USED}
set_instance_parameter_value sysPLL {PORT_PFDENA} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_PHASECOUNTERSELECT} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_PHASEDONE} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_PHASESTEP} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_PHASEUPDOWN} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_PLLENA} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_SCANACLR} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_SCANCLK} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_SCANCLKENA} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_SCANDATA} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_SCANDATAOUT} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_SCANDONE} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_SCANREAD} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_SCANWRITE} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_SCLKOUT0} {}
set_instance_parameter_value sysPLL {PORT_SCLKOUT1} {}
set_instance_parameter_value sysPLL {PORT_VCOOVERRANGE} {}
set_instance_parameter_value sysPLL {PORT_VCOUNDERRANGE} {}
set_instance_parameter_value sysPLL {PORT_clk0} {PORT_USED}
set_instance_parameter_value sysPLL {PORT_clk1} {PORT_USED}
set_instance_parameter_value sysPLL {PORT_clk2} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_clk3} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_clk4} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_clk5} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_clk6} {}
set_instance_parameter_value sysPLL {PORT_clk7} {}
set_instance_parameter_value sysPLL {PORT_clk8} {}
set_instance_parameter_value sysPLL {PORT_clk9} {}
set_instance_parameter_value sysPLL {PORT_clkena0} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_clkena1} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_clkena2} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_clkena3} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_clkena4} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_clkena5} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_extclk0} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_extclk1} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_extclk2} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_extclk3} {PORT_UNUSED}
set_instance_parameter_value sysPLL {PORT_extclkena0} {}
set_instance_parameter_value sysPLL {PORT_extclkena1} {}
set_instance_parameter_value sysPLL {PORT_extclkena2} {}
set_instance_parameter_value sysPLL {PORT_extclkena3} {}
set_instance_parameter_value sysPLL {PRIMARY_CLOCK} {}
set_instance_parameter_value sysPLL {QUALIFY_CONF_DONE} {}
set_instance_parameter_value sysPLL {SCAN_CHAIN} {}
set_instance_parameter_value sysPLL {SCAN_CHAIN_MIF_FILE} {}
set_instance_parameter_value sysPLL {SCLKOUT0_PHASE_SHIFT} {}
set_instance_parameter_value sysPLL {SCLKOUT1_PHASE_SHIFT} {}
set_instance_parameter_value sysPLL {SELF_RESET_ON_GATED_LOSS_LOCK} {}
set_instance_parameter_value sysPLL {SELF_RESET_ON_LOSS_LOCK} {}
set_instance_parameter_value sysPLL {SKIP_VCO} {}
set_instance_parameter_value sysPLL {SPREAD_FREQUENCY} {}
set_instance_parameter_value sysPLL {SWITCH_OVER_COUNTER} {}
set_instance_parameter_value sysPLL {SWITCH_OVER_ON_GATED_LOCK} {}
set_instance_parameter_value sysPLL {SWITCH_OVER_ON_LOSSCLK} {}
set_instance_parameter_value sysPLL {SWITCH_OVER_TYPE} {}
set_instance_parameter_value sysPLL {USING_FBMIMICBIDIR_PORT} {}
set_instance_parameter_value sysPLL {VALID_LOCK_MULTIPLIER} {}
set_instance_parameter_value sysPLL {VCO_DIVIDE_BY} {}
set_instance_parameter_value sysPLL {VCO_FREQUENCY_CONTROL} {}
set_instance_parameter_value sysPLL {VCO_MULTIPLY_BY} {}
set_instance_parameter_value sysPLL {VCO_PHASE_SHIFT_STEP} {}
set_instance_parameter_value sysPLL {WIDTH_CLOCK} {5}
set_instance_parameter_value sysPLL {WIDTH_PHASECOUNTERSELECT} {}

add_instance systemID altera_avalon_sysid_qsys 19.1
set_instance_parameter_value systemID {id} {0}

# exported interfaces
add_interface clk clock sink
set_interface_property clk EXPORT_OF sysClkIn.clk_in
add_interface i2cserialline conduit end
set_interface_property i2cserialline EXPORT_OF i2cMaster.i2c_serial
add_interface ledcntrlreg conduit end
set_interface_property ledcntrlreg EXPORT_OF ledCntrlReg.external_connection
add_interface reset reset sink
set_interface_property reset EXPORT_OF sysClkIn.clk_in_reset
add_interface reset_async conduit end
set_interface_property reset_async EXPORT_OF sysPLL.areset_conduit
add_interface sdramcntrlout conduit end
set_interface_property sdramcntrlout EXPORT_OF sdramCntrl.wire
add_interface statusreg conduit end
set_interface_property statusreg EXPORT_OF statusReg.external_connection
add_interface syspll_clk100 clock source
set_interface_property syspll_clk100 EXPORT_OF sysPLL.c1
add_interface syspll_locked conduit end
set_interface_property syspll_locked EXPORT_OF sysPLL.locked_conduit

# connections and connection parameters
add_connection cpuN2.data_master OCM.s1
set_connection_parameter_value cpuN2.data_master/OCM.s1 arbitrationPriority {1}
set_connection_parameter_value cpuN2.data_master/OCM.s1 baseAddress {0x04004000}
set_connection_parameter_value cpuN2.data_master/OCM.s1 defaultConnection {0}

add_connection cpuN2.data_master cpuN2.debug_mem_slave
set_connection_parameter_value cpuN2.data_master/cpuN2.debug_mem_slave arbitrationPriority {1}
set_connection_parameter_value cpuN2.data_master/cpuN2.debug_mem_slave baseAddress {0x04008800}
set_connection_parameter_value cpuN2.data_master/cpuN2.debug_mem_slave defaultConnection {0}

add_connection cpuN2.data_master i2cMaster.csr
set_connection_parameter_value cpuN2.data_master/i2cMaster.csr arbitrationPriority {1}
set_connection_parameter_value cpuN2.data_master/i2cMaster.csr baseAddress {0x04009000}
set_connection_parameter_value cpuN2.data_master/i2cMaster.csr defaultConnection {0}

add_connection cpuN2.data_master jtagUART.avalon_jtag_slave
set_connection_parameter_value cpuN2.data_master/jtagUART.avalon_jtag_slave arbitrationPriority {1}
set_connection_parameter_value cpuN2.data_master/jtagUART.avalon_jtag_slave baseAddress {0x04009078}
set_connection_parameter_value cpuN2.data_master/jtagUART.avalon_jtag_slave defaultConnection {0}

add_connection cpuN2.data_master ledCntrlReg.s1
set_connection_parameter_value cpuN2.data_master/ledCntrlReg.s1 arbitrationPriority {1}
set_connection_parameter_value cpuN2.data_master/ledCntrlReg.s1 baseAddress {0x04009060}
set_connection_parameter_value cpuN2.data_master/ledCntrlReg.s1 defaultConnection {0}

add_connection cpuN2.data_master sdramCntrl.s1
set_connection_parameter_value cpuN2.data_master/sdramCntrl.s1 arbitrationPriority {1}
set_connection_parameter_value cpuN2.data_master/sdramCntrl.s1 baseAddress {0x02000000}
set_connection_parameter_value cpuN2.data_master/sdramCntrl.s1 defaultConnection {0}

add_connection cpuN2.data_master statusReg.s1
set_connection_parameter_value cpuN2.data_master/statusReg.s1 arbitrationPriority {1}
set_connection_parameter_value cpuN2.data_master/statusReg.s1 baseAddress {0x04009050}
set_connection_parameter_value cpuN2.data_master/statusReg.s1 defaultConnection {0}

add_connection cpuN2.data_master sysPLL.pll_slave
set_connection_parameter_value cpuN2.data_master/sysPLL.pll_slave arbitrationPriority {1}
set_connection_parameter_value cpuN2.data_master/sysPLL.pll_slave baseAddress {0x04009040}
set_connection_parameter_value cpuN2.data_master/sysPLL.pll_slave defaultConnection {0}

add_connection cpuN2.data_master systemID.control_slave
set_connection_parameter_value cpuN2.data_master/systemID.control_slave arbitrationPriority {1}
set_connection_parameter_value cpuN2.data_master/systemID.control_slave baseAddress {0x04009070}
set_connection_parameter_value cpuN2.data_master/systemID.control_slave defaultConnection {0}

add_connection cpuN2.debug_reset_request OCM.reset1

add_connection cpuN2.debug_reset_request cpuN2.reset

add_connection cpuN2.debug_reset_request jtagUART.reset

add_connection cpuN2.debug_reset_request sdramClk.ref_reset

add_connection cpuN2.debug_reset_request sdramCntrl.reset

add_connection cpuN2.debug_reset_request systemID.reset

add_connection cpuN2.instruction_master OCM.s1
set_connection_parameter_value cpuN2.instruction_master/OCM.s1 arbitrationPriority {1}
set_connection_parameter_value cpuN2.instruction_master/OCM.s1 baseAddress {0x04004000}
set_connection_parameter_value cpuN2.instruction_master/OCM.s1 defaultConnection {0}

add_connection cpuN2.instruction_master cpuN2.debug_mem_slave
set_connection_parameter_value cpuN2.instruction_master/cpuN2.debug_mem_slave arbitrationPriority {1}
set_connection_parameter_value cpuN2.instruction_master/cpuN2.debug_mem_slave baseAddress {0x04008800}
set_connection_parameter_value cpuN2.instruction_master/cpuN2.debug_mem_slave defaultConnection {0}

add_connection cpuN2.instruction_master sdramCntrl.s1
set_connection_parameter_value cpuN2.instruction_master/sdramCntrl.s1 arbitrationPriority {1}
set_connection_parameter_value cpuN2.instruction_master/sdramCntrl.s1 baseAddress {0x02000000}
set_connection_parameter_value cpuN2.instruction_master/sdramCntrl.s1 defaultConnection {0}

add_connection cpuN2.irq i2cMaster.interrupt_sender
set_connection_parameter_value cpuN2.irq/i2cMaster.interrupt_sender irqNumber {1}

add_connection cpuN2.irq jtagUART.irq
set_connection_parameter_value cpuN2.irq/jtagUART.irq irqNumber {0}

add_connection sdramClk.reset_source OCM.reset1

add_connection sdramClk.reset_source cpuN2.reset

add_connection sdramClk.reset_source i2cMaster.reset_sink

add_connection sdramClk.reset_source jtagUART.reset

add_connection sdramClk.reset_source ledCntrlReg.reset

add_connection sdramClk.reset_source sdramCntrl.reset

add_connection sdramClk.reset_source statusReg.reset

add_connection sdramClk.reset_source systemID.reset

add_connection sdramClk.sdram_clk sdramCntrl.clk

add_connection sdramClk.sys_clk OCM.clk1

add_connection sdramClk.sys_clk cpuN2.clk

add_connection sdramClk.sys_clk i2cMaster.clock

add_connection sdramClk.sys_clk jtagUART.clk

add_connection sdramClk.sys_clk ledCntrlReg.clk

add_connection sdramClk.sys_clk statusReg.clk

add_connection sdramClk.sys_clk systemID.clk

add_connection sysClkIn.clk sysPLL.inclk_interface

add_connection sysClkIn.clk_reset sysPLL.inclk_interface_reset

add_connection sysPLL.c0 sdramClk.ref_clk

# interconnect requirements
set_interconnect_requirement {$system} {qsys_mm.clockCrossingAdapter} {HANDSHAKE}
set_interconnect_requirement {$system} {qsys_mm.enableEccProtection} {FALSE}
set_interconnect_requirement {$system} {qsys_mm.insertDefaultSlave} {FALSE}
set_interconnect_requirement {$system} {qsys_mm.maxAdditionalLatency} {1}

save_system {nano_i2c_lcd_ss.qsys}

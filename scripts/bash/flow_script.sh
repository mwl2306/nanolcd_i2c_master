#!/bin/bash

#compile consists of:
# Analysis and Elaboration
# Fitter
# Assembler
# Timing Analysis

#will also need a qsys script
#need to figure out how to redirect quartus flow report output
#dont forget to check permissions on scripts
PROJECT_ROOT=/home/micah/git_wa/nanolcd_i2c_master/
PROJECT_NAME=nano_i2c_lcd
GEN_DIR=qsys_gen

#-------------------------------------
#make sure your environment is exposed to the install directory of nios2 eds stuff!
#navigate to wd
cd $PROJECT_ROOT

#create output dir for qsys stuff
echo "================================ create qsys_gen directory ====================================="

if [ ! -d "$GEN_DIR" ]; then
    echo "creating $GEN_DIR in $PROJECT_ROOT"
    mkdir ${GEN_DIR}
fi

echo  "$GEN_DIR already exists, entering $GEN_DIR..."
cd    ${GEN_DIR}

#create the qsys file
echo "================================= Create $PROJECT_NAM qsys file ================================"
qsys-script   --script=../scripts/tcl/${PROJECT_NAME}.tcl

#create the qsys system
echo "================================== Generate VHDL from qsys file ================================"
qsys-generate --synthesis=VHDL ${PROJECT_NAME}_ss.qsys

#return to root directory
cd    ${PROJECT_ROOT}

#now that vhdl is available, and qsys system is generated, analysis and synthesis.. complete flow
echo "========================= Begin Analysis and Synthesis of $PROJECT_NAME ========================"
quartus_map ${PROJECT_NAME}
echo "================================ Analysis and Synthesis Complete ==============================="

echo "======================================== Begin Fit ============================================="
quartus_fit ${PROJECT_NAME}
echo "======================================= Fit Complete ==========================================="

echo "=============================== Begin Static Timing Analysis ==================================="
quartus_sta ${PROJECT_NAME}
echo "======================================= STA Complete ==========================================="

echo "=========================== Begin Assembly (generate programming file)  =========================="
quartus_asm ${PROJECT_NAME}
echo "================================= Programming File Generated ==================================="
